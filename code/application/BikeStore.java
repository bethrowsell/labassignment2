//Beth Rowsell 2135123
package application;

public class BikeStore{
	public static void main(String[] args){
		vehicles.Bicycle[] bike = new vehicles.Bicycle[4];

        bike[0] = new vehicles.Bicycle("BMC", 0, 40);
        bike[1] = new vehicles.Bicycle("Supercycle", 6, 50);
        bike[2] = new vehicles.Bicycle("Trek", 10, 60);
        bike[3] = new vehicles.Bicycle("BMC", 10, 70);

        for(vehicles.Bicycle b : bike){
            System.out.println(b);
        }
	}

}
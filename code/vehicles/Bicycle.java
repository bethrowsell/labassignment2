//Beth Rowsell 2135123
package vehicles;
public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle (String m, int g, double s){
        this.manufacturer = m;
        this.numberGears = g;
        this.maxSpeed = s;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", Max speed: " + this.maxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }
}